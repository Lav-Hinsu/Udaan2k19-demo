package in.ac.bvmengineering.udaan2k19.UI.Fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import in.ac.bvmengineering.udaan2k19.Adapter.EventCategoryAdapter;
import in.ac.bvmengineering.udaan2k19.DataClass.EventCategory;
import in.ac.bvmengineering.udaan2k19.Interface.EventCatagoryClicked;
import in.ac.bvmengineering.udaan2k19.R;

public class Home extends Fragment implements EventCatagoryClicked {

    ArrayList<EventCategory> list;
    RecyclerView recyclerView;
    EventCategoryAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("toolbar");

        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);


        toolbar.setTitle("toolbar");

        CollapsingToolbarLayout ctl = view.findViewById(R.id.collapsing_toolbar);

        ctl.setTitle("collapsing toolbar");

        // recylcer view starts

        recyclerView = view.findViewById(R.id.recycler_view);

        list = new ArrayList<>();

        list.add(new EventCategory(R.drawable.home));
        list.add(new EventCategory(R.drawable.arcamera));
        list.add(new EventCategory(R.drawable.registerd));

        adapter = new EventCategoryAdapter(list, getActivity(), this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

    }

    @Override
    public void respondOnClickEvent(EventCategory obj) {
        switch (obj.getImage()) {
            case R.drawable.home:
                Toast.makeText(getActivity(), "you are in technical", Toast.LENGTH_SHORT).show();
                Objects.requireNonNull(getFragmentManager()).beginTransaction().replace(R.id.frame, new TechnicalEvent()).commit();
                break;
            case R.drawable.arcamera:
                Toast.makeText(getActivity(), "you are in non-technical", Toast.LENGTH_SHORT).show();
                Objects.requireNonNull(getFragmentManager()).beginTransaction().replace(R.id.frame, new NonTechnicalEvent()).commit();
                break;
            case R.drawable.registerd:
                Toast.makeText(getActivity(), "you are in cultural", Toast.LENGTH_SHORT).show();
                Objects.requireNonNull(getFragmentManager()).beginTransaction().replace(R.id.frame, new CulturalEvent()).commit();
                break;
        }



    }
}


