package in.ac.bvmengineering.udaan2k19.UI.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import in.ac.bvmengineering.udaan2k19.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void transferdata(View view) {

        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }
}
