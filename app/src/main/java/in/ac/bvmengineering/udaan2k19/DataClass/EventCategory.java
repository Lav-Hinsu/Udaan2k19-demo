package in.ac.bvmengineering.udaan2k19.DataClass;

public class EventCategory {

    private int image;

    public EventCategory(int image) {

        this.image = image;
    }

    public int getImage() {
        return image;
    }
}
