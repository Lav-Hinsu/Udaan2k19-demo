package in.ac.bvmengineering.udaan2k19.Interface;

import in.ac.bvmengineering.udaan2k19.DataClass.EventCategory;

public interface EventCatagoryClicked {
    void respondOnClickEvent(EventCategory obj);
}
